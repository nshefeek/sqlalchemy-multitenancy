"""
Add tenant specific model

Revision ID: 2a0b0a7107f8
Revises: c3ffb1ceaa98
Create Date: 2023-08-28 19:03:58.339014

"""
import sqlalchemy as sa

from alembic import op
from db import multitenancy

# revision identifiers, used by Alembic.
revision = "2a0b0a7107f8"
down_revision = "c3ffb1ceaa98"
branch_labels = None
depends_on = None


@multitenancy.tenant_migration
def upgrade(schema: str) -> None:
    op.create_table(
        "tenant-specific-model",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_tenant-specific-model")),
        schema=schema,
    )


@multitenancy.tenant_migration
def downgrade(schema: str) -> None:
    op.drop_table("tenant-specific-model", schema=schema)
