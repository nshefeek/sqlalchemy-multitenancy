import asyncio
import sys
from logging.config import fileConfig
from typing import Any, Literal

from sqlalchemy import engine_from_config, pool, select
from sqlalchemy.engine import Connectable, Connection
from sqlalchemy.ext.asyncio import AsyncEngine
from sqlalchemy.sql.ddl import CreateSchema
from sqlalchemy.sql.schema import SchemaItem

import db.models  # noqa: F401
import sentry
from alembic import context
from db import Base
from db._base import TENANT_SCHEMA
from db.models import Tenant
from db.multitenancy import tenant_context_var
from settings import DatabaseSettings, get_settings

sentry.init_sentry()
config = context.config

if config.config_file_name is not None:
    fileConfig(config.config_file_name)

if not config.get_main_option("sqlalchemy.url"):
    config.set_main_option("sqlalchemy.url", get_settings(DatabaseSettings).url)

target_metadata = Base.metadata


def include_public_obj(
    obj: SchemaItem,
    name: str,
    type_: Literal[
        "schema",
        "table",
        "column",
        "index",
        "unique_constraint",
        "foreign_key_constraint",
    ],
    reflected: bool,  # noqa: ARG001, FBT001
    compare_to: SchemaItem | None,  # noqa: ARG001
) -> bool:
    if type_ == "column":
        return True

    if type_ == "table" and name == "alembic_version" and obj.schema == TENANT_SCHEMA:
        return False

    return obj.schema in ("public", TENANT_SCHEMA, None)


def _run_tenant_migration(
    connection: Connection,
    schema: str,
    **kwargs: Any,  # noqa: ANN401
) -> None:
    context.configure(
        connection=connection,
        target_metadata=target_metadata,
        compare_type=True,
        compare_server_default=True,
        version_table_schema=schema,
        **kwargs,
    )
    token = tenant_context_var.set(schema)

    bind = context.get_bind()
    bind.execute(CreateSchema(schema, if_not_exists=True))
    context.run_migrations()

    tenant_context_var.reset(token)


def do_run_migrations(connection: Connection) -> None:
    """
    Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.
    """
    # Public schema migrations
    context.configure(
        connection=connection,
        target_metadata=target_metadata,
        compare_type=True,
        compare_server_default=True,
        include_schemas=True,
        include_object=include_public_obj,
    )
    with context.begin_transaction():
        context.run_migrations()

        # Dirty hack
        if "--autogenerate" in sys.argv:
            return

        _run_tenant_migration(
            connection=connection,
            schema=TENANT_SCHEMA,
            include_schemas=True,
            include_object=include_public_obj,
        )
        for tenant_schema in connection.scalars(select(Tenant.schema)).all():
            _run_tenant_migration(connection=connection, schema=tenant_schema)


async def do_run_migrations_async(connectable: Connectable) -> None:
    async with connectable.connect() as conn:
        await conn.run_sync(do_run_migrations)


def run_migrations() -> None:
    connectable = context.config.attributes.get("connection")
    if not connectable:
        connectable = AsyncEngine(
            engine_from_config(
                config.get_section(config.config_ini_section),
                poolclass=pool.NullPool,
                future=True,
            ),
        )
    if isinstance(connectable, AsyncEngine):
        asyncio.run(do_run_migrations_async(connectable))
    else:
        do_run_migrations(connectable)


run_migrations()
