from __future__ import annotations

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session
from sqlalchemy.schema import CreateSchema

from alembic import command, config
from db.models import Tenant


class MultiTenancyService:
    def __init__(
        self,
        session: AsyncSession,
    ) -> None:
        self._session = session

    async def create_tenant(self, name: str) -> Tenant:
        tenant = Tenant(name=name, schema=f"tenant-{name}")
        self._session.add(tenant)
        await self._session.flush()
        return tenant

    async def create_schema(self, tenant: Tenant) -> None:
        ddl = CreateSchema(
            name=tenant.schema,
            if_not_exists=True,
        )  # type: ignore[no-untyped-call]
        await self._session.execute(ddl)

    async def run_migrations(self) -> None:
        def run_upgrade(session: Session, cfg: config.Config) -> None:
            cfg.attributes["connection"] = session.connection()
            command.upgrade(cfg, "head")

        await self._session.run_sync(run_upgrade, config.Config("alembic.ini"))
