from core.multitenancy.services import MultiTenancyService


class TenantCreateCommand:
    def __init__(self, service: MultiTenancyService) -> None:
        self._service = service

    async def execute(self, name: str) -> None:
        tenant = await self._service.create_tenant(name=name)
        await self._service.create_schema(tenant=tenant)
        await self._service.run_migrations()
