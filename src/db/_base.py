from datetime import datetime

from sqlalchemy import DateTime, MetaData
from sqlalchemy.orm import DeclarativeBase, registry

TENANT_SCHEMA = "__tenant_schema__"

meta = MetaData(
    naming_convention={
        "ix": "ix_%(column_0_label)s",
        "uq": "uq_%(table_name)s_%(column_0_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s",
    },
)


def tenant_metadata(schema_name: str) -> MetaData:
    tenant_meta = MetaData()
    for table in meta.tables.values():
        if table.schema == TENANT_SCHEMA:
            table.to_metadata(tenant_meta, schema=schema_name)
    return tenant_meta


class Base(DeclarativeBase):
    metadata = meta

    registry = registry(
        type_annotation_map={
            datetime: DateTime(timezone=True),
        },
    )
