from sqlalchemy import String
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column

from db import Base
from db._base import TENANT_SCHEMA


class Book(Base):
    __tablename__ = "book"

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str] = mapped_column(String(255), unique=True)


class Tenant(MappedAsDataclass, Base):
    __tablename__ = "tenant"

    id: Mapped[int] = mapped_column(primary_key=True, init=False)
    name: Mapped[str]
    schema: Mapped[str] = mapped_column(unique=True)


class TenantSpecificModel(Base):
    __tablename__ = "tenant-specific-model"
    __table_args__ = {"schema": TENANT_SCHEMA}  # noqa: RUF012

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
