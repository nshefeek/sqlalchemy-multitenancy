from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import APIRouter, Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from adapters.api.dependencies import get_tenant_context
from db.models import TenantSpecificModel

router = APIRouter(
    tags=["tenant-objects"],
    prefix="/tenant-objects",
    dependencies=[Depends(get_tenant_context)],
)


@router.get("")
@inject
async def tenant_obj_create(
    session: Annotated[AsyncSession, Inject],
) -> None:
    stmt = select(TenantSpecificModel)
    print((await session.scalars(stmt)).all())  # noqa: T201
